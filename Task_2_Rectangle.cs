using System;

namespace Program
{
	class Rectangle
	{	
		#region Fields
		
		private double _x;
		private double _y;
		private double _width;
		private double _height;
		
		#endregion
		
		#region Constructors
		
		public Rectangle(double x, double y, double width, double height)
		{
			_x = x;
			_y = y;
			_width = width;
			_height = height;
		}
	
		#endregion
		
		#region Properties
		
		public double X 
		{ 
			get
			{
				return _x;
			} 
			set
			{
				_x = value;
			} 
		}
		
		public double Y 
		{ 
			get
			{
				return _y;
			}			
			set 
			{
				_y = value;
			}
		}
		
		public double Width
		{
			get
			{
				return _width;				
			}
			
			set
			{
				_width = value;
			}
		}
		
		public double Height
		{
			get
			{
				return _height;			
			}
			
			set
			{
				_height = value;				
			}
		}
	
		#endregion
		
		#region Methods		
	
		public void AddWidth(double addWidth)
		{
			_width += addWidth;
		}
		
		public void AddHeight(double addHeight)
		{
			_height += addHeight;
		}
		
		public void New_X_Y(double x, double y)
		{
			_x = x;
			_y = y;
		}
		
		public void MoveRectangle(double move_x, double move_y)
		{
			_x += move_x;
			_y += move_y;
		}
		
		public static bool CheckIntersection(Rectangle rect1, Rectangle rect2)
		{ 
			if(rect1.X + rect1.Width < rect2.X || rect2.X + rect2.Width < rect1.X || rect1.Y + rect1.Height < rect2.Y || rect2.Y + rect2.Height < rect1.Y)
			{
				return false;
			}		
			else 
			{
				return true;
			}
		}
		
		public static Rectangle RectangleFromIntersection(Rectangle rect1, Rectangle rect2)
		{
			if(CheckIntersection(rect1, rect2))
			{
				double x = Math.Max(rect1.X, rect2.X);
				double y = Math.Max(rect1.Y, rect2.Y);
				double width = Math.Abs(x - Math.Min(rect1.X + rect1.Width, rect2.X + rect2.Width));
				double height = Math.Abs(y - Math.Min(rect1.Y + rect1.Height, rect2.Y + rect2.Height));								
				
				return new Rectangle(x, y, width, height);
			}					
			else 
			{
				return null;
			}
		}
		
		public static Rectangle RectangleWithTwoInside(Rectangle rect1, Rectangle rect2)
		{								
			double x1 = (rect1.X < rect2.X) ? rect1.X : rect2.X;
			double y1 = (rect1.Y < rect2.Y) ? rect1.Y : rect2.Y;
			double x2 = (rect1.X + rect1.Width > rect2.X + rect2.Width) ? rect1.X + rect1.Width : rect2.X + rect2.Width;
			double y2 = (rect1.Y + rect1.Height > rect2.Y + rect2.Height) ? rect1.Y + rect1.Height : rect2.Y + rect2.Height;; 			
				
			return new Rectangle(x1, y1, Math.Abs(x1 - x2), Math.Abs(y1 - y2));		
		}

		public override string ToString()
		{
			return String.Format("Bottom-left X: {0}, Bottom-left Y: {1}, Width: {2}, Height: {3}", _x, _y, _width, _height);
		}
		
		#endregion
	}
	
	class RectangleTest
	{
		static void Main()
		{
			Rectangle rect1 = new Rectangle(1, 1, 10, 10);
			Rectangle rect2 = new Rectangle(15, 55, 10, 14);
			
			Rectangle rect3 = Rectangle.RectangleFromIntersection(rect1, rect2);
			if(rect3 != null)
			{
				Console.WriteLine("New rectangle from intersection: {0}", rect3.ToString());
			}			
			else 
			{
				Console.WriteLine("No intersection");	
			}
			
			
			rect3 = Rectangle.RectangleWithTwoInside(rect1, rect2);
			Console.WriteLine("New rectangle from RectangleWithTwoInside: {0}", rect3.ToString());
			
			rect1.MoveRectangle(7, 6);
			Console.WriteLine("Rectangle1 was moved: {0}", rect1.ToString());
			
			rect1.AddWidth(3);
			Console.WriteLine("Rectangle1 width was enlarged: {0}", rect1.ToString());					
			
			Console.ReadKey();
		}
	}	
}