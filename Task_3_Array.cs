using System;

namespace Program
{
	class MyArray
	{	
		#region Fields
		
		private int[] _elements;
		
		#endregion
		
		#region Constructors

		public MyArray(int arraySize)
		{
			_elements = new int [arraySize];
		}
		
		#endregion
		
		#region Properties
		
		public int Length
		{
			get
			{
				return _elements.Length;
			}			
		}
		
		public int this[int index]
		{
			get
			{
				if(index < 0 || index >= _elements.Length)
				{
                    throw new IndexOutOfRangeException();					
				}
				else
				{
					return _elements[index];
				}
			}
			
			set
			{
				if(index < 0 || index >= _elements.Length)
				{
                    throw new IndexOutOfRangeException();					
				}
				else
				{
					_elements[index] = value;
				}
				
			}
		}
	
		#endregion
		
		#region Methods		
	
		public void AddToAll(int number)
		{
			for(var i = 0; i < _elements.Length; i++)
			{
				_elements[i] += number;
			}
		}
		
		public void SubtractFromAll(int number)
		{
			for(var i = 0; i < _elements.Length; i++)
			{
				_elements[i] -= number;
			}
		}
		
		public void MultiplyAll(int number)
		{
			for(var i = 0; i < _elements.Length; i++)
			{
				_elements[i] *= number;
			}
		}
	
		public static MyArray SubArray(MyArray ToCopy, int index, int length)
		{
			if(index + length > ToCopy.Length)
			{
				length = ToCopy.Length - index;
			}			
			
			MyArray arrayCopy = new MyArray(length);
				
			for(var i = 0; i < length; i++)
			{
				arrayCopy[i] = ToCopy[index + i];
			}
				
			return arrayCopy;			
		}
		
		public override string ToString()
		{
			string result = "";
			
			for(var i = 0; i < _elements.Length; i++)
			{
				result += String.Format("{0} ", _elements[i]);
			}
			
			return result;
		}
		
		public static bool Equals(MyArray arr1, MyArray arr2)
		{
			if(arr1.Length == arr2.Length)
			{
				var i = 0;				
				
				while(i != arr1.Length)
				{												
					if(arr1[i] != arr2[i])
					{
						return false;
					}										
					i++;
				}
				
				return true;
			}
			else
			{
				return false;
			}			
		}		
		
		#endregion			
		
		#region Operators
		
		public static MyArray operator +(MyArray arr1, MyArray arr2)
		{
			if(arr1.Length == arr2.Length)
			{								
				for(var i = 0; i < arr1.Length; i++)
				{
					arr1[i] += arr2[i];
				}
				
				return arr1;
			}
			else
			{
				throw new IndexOutOfRangeException();
			}		
		}
		
		public static MyArray operator -(MyArray arr1, MyArray arr2)
		{
			if(arr1.Length == arr2.Length)
			{
				for(var i = 0; i < arr1.Length; i++)
				{
					arr1[i] -= arr2[i];
				}
				
				return arr1;
			}
			else
			{
				throw new IndexOutOfRangeException();
			}		
		}
		
		#endregion
	}
	
	class ArrayTest
	{
		static void Main()
		{
			MyArray arr1 = new MyArray(3);
			arr1[0] = 0;
			arr1[1] = 1;
			arr1[2] = 2;
			
			MyArray arr2 = new MyArray(3);
			arr2[0] = 0;
			arr2[1] = 1;
			arr2[2] = 2;
								
			Console.WriteLine("Is array1 equal array2?: {0}", MyArray.Equals(arr1, arr2));
															
			MyArray subArr = MyArray.SubArray(arr1, 1, 5);
			Console.WriteLine("Subarray from array1: {0}", subArr.ToString());
			
			subArr.AddToAll(5);			
			Console.WriteLine("5 was added to all syb array elements: {0}", subArr.ToString());
			
			subArr.MultiplyAll(3);			
			Console.WriteLine("All elements were multiplied by 3: {0}", subArr.ToString());
			
			arr1 = arr1 + arr2;
			Console.WriteLine("Array1 + array2 = {0}", arr1.ToString());						
					
			Console.ReadKey();
		}
	}	
}